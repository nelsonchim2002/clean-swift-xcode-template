//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class ___VARIABLE_productName___Interactor {
    var presenter: ___VARIABLE_productName___Presenter___VARIABLE_protocolSuffix___?
}

extension ___VARIABLE_productName___Interactor: ___VARIABLE_productName___Interactor___VARIABLE_protocolSuffix___ {

}