//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class ___VARIABLE_productName___Router {
    weak var viewController: UIViewController?
}

extension ___VARIABLE_productName___Router: ___VARIABLE_productName___Router___VARIABLE_protocolSuffix___ {
    
}