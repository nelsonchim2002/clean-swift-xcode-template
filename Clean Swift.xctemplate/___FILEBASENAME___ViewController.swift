//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class ___VARIABLE_productName___ViewController: UIViewController {
    var interactor: ___VARIABLE_productName___Interactor___VARIABLE_protocolSuffix___?
}

extension ___VARIABLE_productName___ViewController: ___VARIABLE_productName___ViewController___VARIABLE_protocolSuffix___ {

}