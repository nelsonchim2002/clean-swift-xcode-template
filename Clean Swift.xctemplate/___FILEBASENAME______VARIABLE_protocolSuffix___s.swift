//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

protocol ___VARIABLE_productName___ViewController___VARIABLE_protocolSuffix___ {
}

protocol ___VARIABLE_productName___Interactor___VARIABLE_protocolSuffix___ {
}

protocol ___VARIABLE_productName___Presenter___VARIABLE_protocolSuffix___ {
}

protocol ___VARIABLE_productName___Router___VARIABLE_protocolSuffix___ {
}

