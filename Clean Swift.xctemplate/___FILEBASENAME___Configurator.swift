//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class ___VARIABLE_productName___Configurator {
    func configViewController() -> UIViewController {
        let viewController = ___VARIABLE_productName___ViewController()
        let interactor = ___VARIABLE_productName___Interactor()
        let presenter = ___VARIABLE_productName___Presenter()
        let router = ___VARIABLE_productName___Router()
        viewController.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = viewController
        presenter.router = router
        router.viewController = viewController

        //Other config

        return viewController
    }
}
