//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class ___VARIABLE_productName___Presenter {
    var viewController: ___VARIABLE_productName___ViewController___VARIABLE_protocolSuffix___?
    var router: ___VARIABLE_productName___Router___VARIABLE_protocolSuffix___?
}

extension ___VARIABLE_productName___Presenter: ___VARIABLE_productName___Presenter___VARIABLE_protocolSuffix___ {

}
