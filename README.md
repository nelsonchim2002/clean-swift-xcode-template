
### Clean Swift Xcode Template

---

## How to set up

1. Clone or download this repository
2. Copy **Clean Swift.xctemplate** to **~/Library/Developer/Xcode/Templates/File Templates**

---

## Create files from Template

To create Clean Swift Module Classes from template

1. Click the **New** button in menu bar, or right click in Project Navigator, select **New File...**.
2. Scroll down and find section **File Templates**.
3. Select **Clean Swift**
4. Type **Module Name** and **Protocol Suffix**
5. Press **Next** and select file destination.
